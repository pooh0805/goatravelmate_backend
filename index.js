const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const { mongoose } = require('./db.js');
var userController = require('./controllers/userController.js');
var beachController = require('./controllers/beachController.js');
var waterfallController = require('./controllers/waterfallController.js');

require('./config/config');

var app = express();

//MIDDLEWARE
app.use(bodyParser.json());
app.use(cors({ origin : 'http://localhost:4200' }));

//Start Server
app.listen(process.env.PORT, () => console.log(`Server started at port : ${process.env.PORT}`));

app.use('/users',userController);
app.use('/beaches',beachController);
app.use('/waterfalls',waterfallController);